package agt

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"sync"

	"gitlab.utc.fr/brezetel/ia04-vote_office/comsoc"
)

var (
	randomNb int
	lastID   int
	idMutex  sync.Mutex
)

type AgentId int
type Agent struct {
	AgentId string
	Name    string
	Prefs   []comsoc.Alternative
	Url     string
	ServId  string
	Options []int
}

type AgentI interface {
	Equal(ag AgentI) bool
	DeepEqual(ag AgentI) bool
	Clone() AgentI
	String() string
	Prefers(a comsoc.Alternative, b comsoc.Alternative) bool
	Start()
}

func NewID() string {
	idMutex.Lock()
	defer idMutex.Unlock()
	lastID++
	return strconv.Itoa(lastID)
}
func NewAgent(id string, name string, prefs []comsoc.Alternative, url string, servId string, option []int) *Agent {
	return &Agent{
		AgentId: id,
		Name:    name,
		Prefs:   prefs,
		Url:     url,
		ServId:  servId,
		Options: option,
	}
}

func remove(slice []comsoc.Alternative, s int) []comsoc.Alternative {
	return append(slice[:s], slice[s+1:]...)
}

func CreateRandomAgent(liste []comsoc.Alternative, url string, servId string) *Agent {
	var listPref []comsoc.Alternative
	copiedList := make([]comsoc.Alternative, len(liste))
	copy(copiedList, liste)
	long := len(liste)
	for i := 0; i < long; i++ {
		random := rand.Intn(len(copiedList))
		listPref = append(listPref, copiedList[random])
		copiedList = remove(copiedList, random)
	}
	name := "Random_Agent_" + strconv.Itoa(randomNb)
	var option []int
	option = append(option, 3)
	randomNb++
	fmt.Println("On crée l'agent avec le nom", name, "et la liste de pref", listPref)
	return NewAgent(NewID(), name, listPref, url, servId, option)

}

// func (rca *Agent) doVote() (aVote bool, err error) {

// 	aVote = false
// 	url := rca.Url + "/vote"

// 	var intSlice []int
// 	for _, val := range rca.Prefs {
// 		intSlice = append(intSlice, int(val))
// 	}
// 	req := comsoc.RequestVote{
// 		Agent_id: strconv.Itoa(int(rca.ID)),
// 		Vote_id:  "1",
// 		Prefs:    intSlice,
// 		Option:   []int{0},
// 	}

// 	data, _ := json.Marshal(req)

// 	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

//		// traitement de la réponse
//		if err != nil {
//			return
//		}
//		if resp.StatusCode != http.StatusOK {
//			err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
//			return
//		} else {
//			aVote = true
//		}
//		return
//	}

func (rca *Agent) doVoteBallot() (aVote bool, err error) {

	aVote = false
	url := rca.Url + "/vote"

	var intSlice []int
	for _, val := range rca.Prefs {
		intSlice = append(intSlice, int(val))
	}
	req := comsoc.RequestVote{
		Agent_id: rca.AgentId,
		Vote_id:  rca.ServId,
		Prefs:    intSlice,
		Option:   []int{3},
	}

	data, _ := json.Marshal(req)

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
	//dataResp, _ := rca.decodeResp(resp)

	if resp.StatusCode != http.StatusOK {
		//log.Println(dataResp)
		bodyBytes, _ := ioutil.ReadAll(resp.Body)
		bodyString := string(bodyBytes)
		err = fmt.Errorf("[%d] %s: %s", resp.StatusCode, resp.Status, bodyString)
		return
	} else {
		aVote = true
		log.Printf("L'agent : %s a voté ", rca.Name)
	}
	return
}

// func (rca *Agent) treatResponse(r *http.Response) int {
// 	buf := new(bytes.Buffer)
// 	buf.ReadFrom(r.Body)

// 	var resp comsoc.Response
// 	json.Unmarshal(buf.Bytes(), &resp)

// 	return resp.Winner
// }

// ne fais qu'une verification que sur l'ID
func (a *Agent) Equal(ag AgentI) bool {
	otherAgent, ok := ag.(*Agent)
	if !ok {
		return false
	}
	return a.AgentId == otherAgent.AgentId
}

// Fais une verif sur tous les attributs des deux agents égalment leurs préfs
func (a *Agent) DeepEqual(ag AgentI) bool {
	otherAgent, ok := ag.(*Agent)
	if !ok {
		return false
	}
	if a.AgentId != otherAgent.AgentId || a.Name != otherAgent.Name || len(a.Prefs) != len(otherAgent.Prefs) {
		return false
	}
	for i, pref := range a.Prefs {
		if pref != otherAgent.Prefs[i] {
			return false
		}
	}
	return true
}

// clone l'agent passé en param
func (a *Agent) Clone() AgentI {
	clonePrefs := make([]comsoc.Alternative, len(a.Prefs))
	copy(clonePrefs, a.Prefs)
	return &Agent{
		a.AgentId,
		a.Name,
		clonePrefs,
		a.Url,
		a.ServId,
		a.Options,
	}
}

// Permet de faire un affichage de l'agent en question
func (a *Agent) String() string {
	return fmt.Sprintf("L'ID de l'agent est : %s, Son Nom est : %s, Ses prefs sont : %v", a.AgentId, a.Name, a.Prefs)
}

// permet de determiner quel alternative l'agent préfere
func (a *Agent) Prefers(first comsoc.Alternative, second comsoc.Alternative) bool {
	for _, pref := range a.Prefs {
		if pref == first {
			return true
		}
		if pref == second {
			return false
		}
	}
	return false
}

func (a *Agent) Start() {
	log.Printf("démarrage de l'agent %s avec l'id : %s", a.Name, a.AgentId)
	log.Printf("L'agent %s essaye de voter ", a.Name)
	_, err := a.doVoteBallot()

	if err != nil {
		log.Println(a.AgentId, "error:", err.Error())
		log.Printf("L'agent %s n'a pas réussi à voter ", a.Name)
	}

}
