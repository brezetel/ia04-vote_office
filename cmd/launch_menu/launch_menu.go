package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.utc.fr/brezetel/ia04-vote_office/agt"
	"gitlab.utc.fr/brezetel/ia04-vote_office/comsoc"
	"gitlab.utc.fr/brezetel/ia04-vote_office/srvr"
)

func decodeResponseBallot(r *http.Response) (req comsoc.NewBallotResponse, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req) // met dans req la réponse du reader de la requete et si erreur enregistre l'erreur
	return
}
func decodeResponseWinner(r *http.Response) (req comsoc.ResponseWinner, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req) // met dans req la réponse du reader de la requete et si erreur enregistre l'erreur
	return
}

// func premiertest() {

// 	const url1 = ":8080"
// 	const url2 = "http://localhost:8080"
// 	const url3 = url2 + "/new_ballot"
// 	const url4 = url2 + "/result"
// 	deadLine := time.Now().In(time.FixedZone("UTC+1", 60*60)).Add(time.Hour).Format(time.RFC3339)

// 	// tests pour créer deux nouveaux votes
// 	req := comsoc.RequestNewBallot{
// 		Rule:     "approval",
// 		Deadline: deadLine,
// 		VoterIDs: []string{"1", "2", "3"},
// 		Alts:     3,
// 		TieBreak: []int{2, 3, 1, 4, 5, 6},
// 	}
// 	req2 := comsoc.RequestNewBallot{
// 		Rule:     "caca",
// 		Deadline: deadLine,
// 		VoterIDs: []string{"1", "2", "3"},
// 		Alts:     12,
// 		TieBreak: []int{1, 2, 3, 4, 5, 6},
// 	}

// 	data, _ := json.Marshal(req)
// 	data2, _ := json.Marshal(req2)

// 	comsoc.AltsGlobal = []comsoc.Alternative{1, 2, 3}
// 	servAgt := srvr.NewRestServerAgent(url1)
// 	log.Println("démarrage du serveur...")
// 	go servAgt.Start()

// 	resp, err := http.Post(url3, "application/json", bytes.NewBuffer(data))
// 	if err != nil {
// 		fmt.Println(err.Error())
// 		return
// 	}
// 	fmt.Println(resp.Status)

// 	resp2, err := http.Post(url3, "application/json", bytes.NewBuffer(data2))
// 	if err != nil {
// 		fmt.Println(err.Error())
// 		return
// 	}
// 	fmt.Println(resp2.Status)

// 	r, _ := decodeResponseBallot(resp)
// 	r2, _ := decodeResponseBallot(resp2)

// 	fmt.Println("Id des bureaux de vote ouverts :")
// 	fmt.Println(r.BallotId)
// 	fmt.Println(r2.BallotId)

// 	//test de création des agents
// 	var newAgt agt.Agent = agt.Agent{
// 		ID:    agt.AgentId(1),
// 		Name:  "Dima",
// 		Prefs: []comsoc.Alternative{2, 3, 1},
// 		Url:   url2,
// 	}
// 	newAgt.Start()

// 	var newAgt2 agt.Agent = agt.Agent{
// 		ID:    agt.AgentId(2),
// 		Name:  "Eliott",
// 		Prefs: []comsoc.Alternative{2, 1, 3},
// 		Url:   url2,
// 	}
// 	newAgt2.Start()

// 	fmt.Println(" On demande au bureau de vote scrutin0 les résultats ")
// 	req3 := comsoc.RequestResult{
// 		Ballotid: "scrutin0",
// 	}
// 	data3, _ := json.Marshal(req3)

// 	resp3, err := http.Post(url4, "application/json", bytes.NewBuffer(data3))
// 	if err != nil {
// 		fmt.Println(err.Error())
// 		return
// 	}
// 	fmt.Println(resp3.Status)
// 	r3, _ := decodeResponseWinner(resp3)
// 	fmt.Print("le gagnant est : ")
// 	fmt.Println(r3.Winner)
// 	fmt.Println("L'ordre est : ")
// 	fmt.Println(r3.Ranking)

// 	// var listeAlternative = []comsoc.Alternative{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
// 	// for i := 0; i < 10; i++ {
// 	// 	agt.CreateRandomAgent(listeAlternative, url2)
// 	// }

// 	fmt.Scanln()

// }

func printMenu() {
	fmt.Println("")
	fmt.Println("========================================================")
	fmt.Println("1: Démarrer le serveur")
	fmt.Println("2: Créer un bureau de vote")
	fmt.Println("3: Créer et faire voter un agent")
	// fmt.Println("4: Générer n agents aléatoires qui vont voter")
	fmt.Println("4: Demander les résultats d'un des bureaux de votes")
	fmt.Println("5: Sortir du programme")
	fmt.Println("Veuillez choisir une option : ")
	fmt.Println("========================================================")
}

func printWelcomeMessage() {
	fmt.Println(`
  __          __  _                                     
  \ \        / / | |                          
   \ \  /\  / /__| | ___ ___  ___ ___   __  
    \ \/  \/ / _ \ |/ __/ _ \| _ \ _ \ / _ \  
     \  /\  /  __/ | (_| (_) | | | | | |  __/   
      \/  \/ \___|_|\___\___/|_| |_| |_|\___|   
	`)

	fmt.Println("Bienvenue dans le système de vote !")
	fmt.Println("Sélectionnez une option pour commencer...")
}
func printServerStartMessage(url string) {
	fmt.Println(`
	__ _             _   _             
	/ _\ |_ __ _ _ __| |_(_)_ __   __ _ 
	\ \| __/ _  | |__| __| |  _ \ / _  |
	_\ \ || (_| | |  | |_| | | | | (_| |
	\__/\__\__,_|_|   \__|_|_| |_|\__, |
	                              |___/
  
	
								  `)
	fmt.Println("Le serveur est démarré et écoute sur", url)
}
func printResultMessage() {
	fmt.Println(`
	
 _ __ ___  ___ _   _| | |_ 
| '__/ _ \/ __| | | | | __|
| | |  __/\__ \ |_| | | |_ 
|_|  \___||___/\__,_|_|\__|
  
	
								  `)

}
func printCreation() {
	fmt.Println(`
     ___               _   _             
    / __\ __ ___  __ _| |_(_)_ __   __ _ 
   / / | '__/ _ \/ _  | __| | '_ \ / _  |
  / /__| | |  __/ (_| | |_| | | | | (_| |
  \____/_|  \___|\__,_|\__|_|_| |_|\__, |
                                   |___/ 
	
								  `)
	fmt.Println("")
	fmt.Println("============================================================")
}

func startServer() {

	url := ":8080"
	servAgt := srvr.NewRestServerAgent(url)

	printServerStartMessage(url)

	go func() {
		log.Fatal(servAgt.Start())
	}()

	time.Sleep(2 * time.Second)
}

func createBallot() {
	printCreation()

	fmt.Println("Création d'un bureau de vote")
	reader := bufio.NewReader(os.Stdin)

	fmt.Println("Entrez la règle de vote :")
	rule, _ := reader.ReadString('\n')

	fmt.Println("Entrez le nombre d'alternatives :")
	altsStr, _ := reader.ReadString('\n')
	alts, _ := strconv.Atoi(strings.TrimSpace(altsStr))

	fmt.Println("Combien de votants y a-t-il ?")
	voterCountStr, _ := reader.ReadString('\n')
	voterCount, _ := strconv.Atoi(strings.TrimSpace(voterCountStr))

	voterIDs := make([]string, voterCount)
	fmt.Println("Entrez les IDs des votants :")
	for i := 0; i < voterCount; i++ {
		fmt.Printf("ID du votant %d : ", i+1)
		voterIDs[i], _ = reader.ReadString('\n')
		voterIDs[i] = strings.TrimSpace(voterIDs[i])
	}

	tieBreak := make([]int, alts)
	fmt.Println("Entrez la règle de tie break (séquence d'entiers) :")
	for i := 0; i < alts; i++ {
		fmt.Printf("Position %d : ", i+1)
		tieBreakStr, _ := reader.ReadString('\n')
		tieBreak[i], _ = strconv.Atoi(strings.TrimSpace(tieBreakStr))
	}
	fmt.Println("Comment souhaitez-vous définir la deadline ?")
	fmt.Println("1. Ajouter une durée à partir de maintenant.")
	fmt.Println("2. Définir une date précise.")
	choiceStr, _ := reader.ReadString('\n')
	choice, _ := strconv.Atoi(strings.TrimSpace(choiceStr))

	var deadline time.Time
	var error error
	switch choice {
	case 1:
		fmt.Println("Entrez la durée à ajouter (en minutes) :")
		durationStr, _ := reader.ReadString('\n')
		duration, _ := strconv.Atoi(strings.TrimSpace(durationStr))
		deadline = time.Now().Add(time.Duration(duration) * time.Minute)
	case 2:
		fmt.Println("Entrez la date précise (En format RFC3339: YYYY-MM-DDTHH:MM:SS+HH:MM) :")
		dateStr, _ := reader.ReadString('\n')
		deadline, error = time.Parse(time.RFC3339, strings.TrimSpace(dateStr))
		if error != nil {
			fmt.Println("Erreur lors de l'analyse de la date :", error)
			return
		}
	default:
		fmt.Println("Choix invalide.")
		return
	}

	req := comsoc.RequestNewBallot{
		Rule:     strings.TrimSpace(rule),
		Deadline: deadline.Format(time.RFC3339),
		VoterIDs: voterIDs,
		Alts:     alts,
		TieBreak: tieBreak,
	}

	data, err := json.Marshal(req)
	if err != nil {
		fmt.Println("Erreur lors de la création du JSON : ", err.Error())
		return
	}

	url := "http://localhost:8080/new_ballot"

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
	if err != nil {
		fmt.Println("Erreur lors de l'envoi de la requête : ", err.Error())
		return
	}
	defer resp.Body.Close()

	fmt.Println("Statut de la réponse :", resp.Status)
	if resp.StatusCode == http.StatusCreated {
		dataResp, _ := decodeResponseBallot(resp)
		fmt.Println("L'id du serveur crée : ", dataResp.BallotId)
	}

	time.Sleep(2 * time.Second)
}

func createAgent() {

	printCreation()
	fmt.Println("Création d'un nouvel agent...")

	reader := bufio.NewReader(os.Stdin)

	fmt.Println("Entrez l'ID de l'agent:")
	idStr, _ := reader.ReadString('\n')
	idStr = strings.TrimSpace(idStr)

	fmt.Println("Entrez le nom de l'agent:")
	name, _ := reader.ReadString('\n')
	name = strings.TrimSpace(name)

	url := "http://localhost:8080"

	fmt.Println("Entrez l'iD du bureau de vote sur lequel l'agent va voter")
	ballotId, _ := reader.ReadString('\n')
	ballotId = strings.TrimSpace(ballotId)

	fmt.Println("Combien d'alternatives y a-t-il ?")
	altsCountStr, _ := reader.ReadString('\n')
	altsCount, _ := strconv.Atoi(strings.TrimSpace(altsCountStr))

	prefs := make([]comsoc.Alternative, altsCount)
	fmt.Println("Entrez les préférences de l'agent (séquence d'entiers) :")
	for i := 0; i < altsCount; i++ {
		fmt.Printf("Préférence %d : ", i+1)
		prefStr, _ := reader.ReadString('\n')
		pref, _ := strconv.Atoi(strings.TrimSpace(prefStr))
		prefs[i] = comsoc.Alternative(pref)
	}

	fmt.Println("L'agent a-t-il des options ? ( notamment pour le vote approval ) (y/n)")
	optionChoice, _ := reader.ReadString('\n')
	optionChoice = strings.TrimSpace(optionChoice)

	var options []int
	if strings.ToLower(optionChoice) == "y" || strings.ToLower(optionChoice) == "yes" {
		fmt.Println("Combien d'options y a-t-il ?")
		optionsCountStr, _ := reader.ReadString('\n')
		optionsCount, _ := strconv.Atoi(strings.TrimSpace(optionsCountStr))

		options = make([]int, optionsCount)
		for i := 0; i < optionsCount; i++ {
			fmt.Printf("Entrez l'option %d : ", i+1)
			optionStr, _ := reader.ReadString('\n')
			option, _ := strconv.Atoi(strings.TrimSpace(optionStr))
			options[i] = option
		}
	}

	newAgt := agt.Agent{
		AgentId: idStr,
		Name:    name,
		Prefs:   prefs,
		Url:     url,
		ServId:  ballotId,
		Options: options,
	}

	fmt.Println("Démarrage de l'agent...")
	newAgt.Start()

}

func generateRandomAgents() {

	fmt.Println("Génération d'agents aléatoires...")

	reader := bufio.NewReader(os.Stdin)

	fmt.Println("Combien d'agents voulez-vous générer ?")
	nAgentsStr, _ := reader.ReadString('\n')
	nAgents, _ := strconv.Atoi(strings.TrimSpace(nAgentsStr))

	fmt.Println("Sur quelle URL les agents doivent-ils écouter ?")
	url, _ := reader.ReadString('\n')
	url = strings.TrimSpace(url)

	fmt.Println("Sur quel ID de bureau de vote les agents doivent-ils voter ?")
	servId, _ := reader.ReadString('\n')
	servId = strings.TrimSpace(servId)

	fmt.Println("Combien d'alternatives y a-t-il ?")
	nAltsStr, _ := reader.ReadString('\n')
	nAlts, _ := strconv.Atoi(strings.TrimSpace(nAltsStr))

	altList := make([]comsoc.Alternative, nAlts)
	for i := range altList {
		altList[i] = comsoc.Alternative(i + 1)
	}

	for i := 0; i < nAgents; i++ {
		agent := agt.CreateRandomAgent(altList, url, servId)
		fmt.Println("Démarrage de l'agent", agent.Name, "...")
		agent.Start()
		fmt.Println("Agent", agent.Name, "a démarré.")
	}
}

func requestResults() {
	printResultMessage()
	// Logique pour demander les résultats
	fmt.Println("Demande des résultats du bureau de vote...")

	// Création du lecteur de ligne pour l'entrée utilisateur
	reader := bufio.NewReader(os.Stdin)

	// Demander l'ID du bureau de vote
	fmt.Println("Entrez l'ID du bureau de vote pour lequel vous voulez les résultats:")
	ballotId, _ := reader.ReadString('\n')
	ballotId = strings.TrimSpace(ballotId)

	url := "http://localhost:8080/result"

	// Créer la demande de résultat
	req := comsoc.RequestResult{
		Ballotid: ballotId,
	}
	data, err := json.Marshal(req)
	if err != nil {
		fmt.Println("Erreur lors de la conversion en JSON:", err)
		return
	}

	// Envoyer la demande de résultat au bureau de vote
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
	if err != nil {
		fmt.Println("Erreur lors de l'envoi de la requête:", err)
		return
	}
	defer resp.Body.Close() // Ne pas oublier de fermer le body de la réponse

	fmt.Println("Statut de la réponse:", resp.Status)

	// Décoder la réponse et afficher les résultats
	result, err := decodeResponseWinner(resp)

	if err != nil {
		fmt.Println("Erreur lors de la lecture de la réponse")
		return
	}

	fmt.Print("Le gagnant est : ")
	fmt.Println(result.Winner)
	fmt.Print("L'ordre est : ")
	fmt.Println(result.Ranking)
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	printWelcomeMessage()
	for {

		printMenu()

		choice, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println("Erreur de lecture:", err)
			continue
		}

		choice = strings.TrimSpace(choice)

		switch choice {
		case "1":
			// Démarrez le serveur
			startServer()
		case "2":
			// Créez un bureau de vote
			createBallot()
		case "3":
			// Créez un nouvel agent
			createAgent()
		// case "4":
		// 	// Générer n agents aléatoires qui vont voter
		// 	generateRandomAgents()
		case "4":
			// Demandez les résultats
			requestResults()
		case "5":
			// Quitter le programme
			fmt.Println("Au revoir !")
			return
		default:
			fmt.Println("Choix invalide. Veuillez essayer à nouveau.")
		}
	}
}
