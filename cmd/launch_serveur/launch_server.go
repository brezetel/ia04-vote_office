package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"gitlab.utc.fr/brezetel/ia04-vote_office/srvr"
)

func printServerStartMessage(url string) {
	fmt.Println(`
	__ _             _   _             
	/ _\ |_ __ _ _ __| |_(_)_ __   __ _ 
	\ \| __/ _  | |__| __| |  _ \ / _  |
	_\ \ || (_| | |  | |_| | | | | (_| |
	\__/\__\__,_|_|   \__|_|_| |_|\__, |
	                              |___/
  
	
								  `)
	fmt.Println("Le serveur est démarré et écoute sur", url)
}
func startServer() {

	url := ":8080"
	servAgt := srvr.NewRestServerAgent(url)

	printServerStartMessage(url)

	go func() {
		log.Fatal(servAgt.Start())
	}()

	time.Sleep(2 * time.Second)
}
func main() {
	startServer()
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Println("Appuyez sur q pour fermer le serveur")
		choice, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println("Erreur de lecture:", err)
			continue
		}
		choice = strings.TrimSpace(choice)

		if choice == "q" {
			fmt.Println("Au Revoir!")
			return
		}
	}
}
