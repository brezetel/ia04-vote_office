// version 2.0.0

package comsoc

import "testing"

func TestTieBreakFactory(t *testing.T) {

	order := []Alternative{
		2, 3, 1,
	}
	tb := TieBreakFactory(order)
	newliste := []Alternative{
		1, 2, 3,
	}
	alt, _ := tb(newliste)

	if alt != 2 {
		t.Errorf("faux ! ")
	}

}

func TestSWFFactory(t *testing.T) {
	AltsGlobal = []Alternative{1, 2, 3}
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
		{3, 2, 1},
	}

	order := []Alternative{
		2, 3, 1,
	}
	tb := TieBreakFactory(order)

	newSwf := SWFFactory(BordaSWF, tb, order)

	alts, _ := newSwf(prefs)

	if alts[0] != 2 {
		t.Errorf("faux ! ")
	}
}
func TestSCFFactory(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
		{3, 2, 1},
	}

	order := []Alternative{
		2, 3, 1,
	}
	tb := TieBreakFactory(order)

	newScf := SCFFactory(MajoritySCF, tb)

	alts, _ := newScf(prefs)

	if alts != 3 {
		t.Errorf("faux ! ")
	}
}

func TestCopelandSWF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	res, _ := CopelandSWF(prefs)

	if res[1] != 2 {
		t.Errorf("error, result for 1 should be 2, %d computed", res[1])
	}
	if res[2] != 1 {
		t.Errorf("error, result for 2 should be 1, %d computed", res[2])
	}
	if res[3] != 0 {
		t.Errorf("error, result for 3 should be 0, %d computed", res[3])
	}

}

func TestCopelandSCF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 4}, // le 4 n est pas censé être sélectionné BORDEL
	}

	res, err := CopelandSCF(prefs)

	if err != nil {
		t.Error(err)
	} else {
		if len(res) != 1 || res[0] != 1 {
			t.Errorf("error, 1 should be the only best Alternative")
		}
	}

}
func TestSTVSWF(t *testing.T) {
	prefs := [][]Alternative{
		{2, 3, 1},
		{2, 1, 3},
		{1, 2, 3},
		{3, 1, 2},
	}
	tb := []Alternative{1, 2, 3}
	res, _ := STV_SWF(prefs, TieBreakSTVFactory(tb))

	if res[2] != 2 {
		t.Errorf("error, result for 2 should be 2, %d computed", res[2])
	}
	if res[1] != 0 {
		t.Errorf("error, result for 1 should be 0, %d computed", res[1])
	}
	if res[3] != 1 {
		t.Errorf("error, result for 3 should be 1, %d computed", res[3])
	}
}
func TestSTVSCF(t *testing.T) {
	prefs := [][]Alternative{
		{3, 2, 1},
		{2, 1, 3},
		{2, 3, 1},
	}
	tb := []Alternative{1, 2, 3}
	res, err := STV_SCF(prefs, TieBreakFactory(tb))

	if err != nil {
		t.Error(err)
	}

	if len(res) != 1 || res[0] != 1 {
		t.Errorf("error, 1 should be the only best Alternative")
	}
}

func TestBordaSWF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	res, _ := BordaSWF(prefs)

	if res[1] != 4 {
		t.Errorf("error, result for 1 should be 4, %d computed", res[1])
	}
	if res[2] != 3 {
		t.Errorf("error, result for 2 should be 3, %d computed", res[2])
	}
	if res[3] != 2 {
		t.Errorf("error, result for 3 should be 2, %d computed", res[3])
	}
}

func TestBordaSCF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	res, err := BordaSCF(prefs)

	if err != nil {
		t.Error(err)
	}

	if len(res) != 1 || res[0] != 1 {
		t.Errorf("error, 1 should be the only best Alternative")
	}
}

func TestMajoritySWF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	res, _ := MajoritySWF(prefs)

	if res[1] != 2 {
		t.Errorf("error, result for 1 should be 2, %d computed", res[1])
	}
	if res[2] != 0 {
		t.Errorf("error, result for 2 should be 0, %d computed", res[2])
	}
	if res[3] != 1 {
		t.Errorf("error, result for 3 should be 1, %d computed", res[3])
	}
}

func TestMajoritySCF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	res, err := MajoritySCF(prefs)

	if err != nil {
		t.Error(err)
	}

	if len(res) != 1 || res[0] != 1 {
		t.Errorf("error, 1 should be the only best Alternative")
	}
}

func TestApprovalSWF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 3, 2},
		{2, 3, 1},
	}
	thresholds := []int{2, 1, 2}

	res, _ := ApprovalSWF(prefs, thresholds)

	if res[1] != 2 {
		t.Errorf("error, result for 1 should be 2, %d computed", res[1])
	}
	if res[2] != 2 {
		t.Errorf("error, result for 2 should be 2, %d computed", res[2])
	}
	if res[3] != 1 {
		t.Errorf("error, result for 3 should be 1, %d computed", res[3])
	}
}

func TestApprovalSCF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 3, 2},
		{1, 2, 3},
		{2, 1, 3},
	}
	thresholds := []int{2, 1, 2}

	res, err := ApprovalSCF(prefs, thresholds)

	if err != nil {
		t.Error(err)
	}
	if len(res) != 1 || res[0] != 1 {
		t.Errorf("error, 1 should be the only best Alternative")
	}
}

func TestCondorcetWinner(t *testing.T) {
	prefs1 := [][]Alternative{
		{1, 2, 3},
		{1, 3, 2},
		{3, 2, 1},
	}

	prefs2 := [][]Alternative{
		{1, 2, 3},
		{2, 3, 1},
		{3, 1, 2},
	}

	res1, _ := CondorcetWinner(prefs1)
	res2, _ := CondorcetWinner(prefs2)

	if len(res1) == 0 || res1[0] != 1 {
		t.Errorf("error, 1 should be the only best alternative for prefs1")
	}
	if len(res2) != 0 {
		t.Errorf("no best alternative for prefs2")
	}
}
