package comsoc

func STV_SWF(p Profile, tb func([]Alternative) (Alternative, error)) (count Count, err error) {

	p2 := p
	count = make(Count)

	nbAlternatives := len(p[0])

	for i := 0; i < nbAlternatives; i++ {
		count2 := make(Count)
		for _, v := range p2 {
			for _, v2 := range v {
				count2[v2] = 0
			}
		}
		for _, v := range p2 {
			count2[v[0]] += 1
		}

		min := MinCount(count2)
		altElimin, _ := tb(min)
		count[altElimin] = i
		for y := range p2 {
			indice := rank(altElimin, p2[y])
			p2[y] = append(p2[y][:indice], p2[y][(indice+1):]...)
		}
	}
	return

}
func STV_SCF(p Profile, tb func([]Alternative) (Alternative, error)) (bestAlts []Alternative, err error) {
	err = CheckProfileAlternative(p, AltsGlobal)
	if err != nil {
		return
	}
	stv_scf, _ := STV_SWF(p, tb)
	bestAlts = MinCount(stv_scf)
	return
}
