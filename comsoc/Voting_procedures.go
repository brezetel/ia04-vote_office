package comsoc

func SWF(p Profile) (count Count, err error) {

	count = make(Count)
	for _, i := range p[0] {
		count[i] = 0
	}
	for _, v := range p {
		count[v[0]] += 1
	}
	return
}
func SCF(p Profile) (bestAlts []Alternative, err error) {
	mp, err := SWF(p)
	if err != nil {
		return
	}
	bestAlts = MaxCount(mp)
	return
}
