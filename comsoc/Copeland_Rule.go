package comsoc

func CopelandSWF(p Profile) (count Count, err error) {

	listeAlternatives := p[0]

	count = make(Count)
	for _, j := range p[0] {
		count[j] = 0
	}
	for _, uneAlternative := range listeAlternatives {
		for _, uneAutreAlternative := range listeAlternatives {
			winner := 0
			loser := 0
			if uneAlternative != uneAutreAlternative {
				for _, v := range p {
					if isPref(uneAlternative, uneAutreAlternative, v) {
						winner += 1
					} else {
						loser += 1
					}
				}
			}
			if winner > loser {
				count[uneAlternative] += 1
			}
		}

	}

	return
}
func CopelandSCF(p Profile) (bestAlts []Alternative, err error) {
	err = CheckProfileAlternative(p, AltsGlobal)
	if err != nil {
		return
	}

	copelandScf, _ := CopelandSWF(p)
	bestAlts = MaxCount(copelandScf)
	return
}
