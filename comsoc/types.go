package comsoc

type RequestNewBallot struct {
	Rule     string   `json:"rule"`
	Deadline string   `json:"deadline"`
	VoterIDs []string `json:"voter-ids"`
	Alts     int      `json:"#alts"`
	TieBreak []int    `json:"tie-break"`
}
type RequestVote struct {
	Agent_id string `json:"agent-id"`
	Vote_id  string `json:"ballot-id"`
	Prefs    []int  `json:"prefs"`
	Option   []int  `json:"options"`
}
type RequestResult struct {
	Ballotid string `json:"ballot_id"`
}

type ResponseWinner struct {
	Winner  int   `json:"winner"`
	Ranking []int `json:"ranking"`
}

type NewBallotResponse struct {
	BallotId string `json:"ballot-id"`
}
