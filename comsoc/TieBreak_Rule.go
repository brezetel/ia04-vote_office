package comsoc

import (
	"errors"
)

func TieBreak(alts []Alternative) (alt Alternative, err error) {

	if len(alts) == 0 {
		err = errors.New("liste d'alternatives nulle")
		return
	}
	alt = alts[0]
	return
}

func TieBreakFactory(orderedAlts []Alternative) func(alts []Alternative) (Alternative, error) {

	f := func(alts []Alternative) (alt Alternative, err error) {

		if len(alts) == 0 {
			err = errors.New("liste d'alternatives nulle")
			return
		}
		for _, v := range orderedAlts {
			for _, v2 := range alts {
				if v == v2 {
					return v, nil
				}
			}
		}
		return 0, errors.New("les alternatives ne correspondent pas")
	}

	return f
}

func TieBreakSTVFactory(orderedAlts []Alternative) func(alts []Alternative) (Alternative, error) {

	f := func(alts []Alternative) (alt Alternative, err error) {

		if len(alts) == 0 {
			err = errors.New("liste d'alternatives nulle")
			return
		}

		for i := len(orderedAlts) - 1; i >= 0; i-- {
			for _, v2 := range alts {
				if orderedAlts[i] == v2 {
					return orderedAlts[i], nil
				}
			}
		}
		return 0, errors.New("les alternatives ne correspondent pas")
	}

	return f
}

func SWFFactory(swf func(p Profile) (Count, error), tb func([]Alternative) (Alternative, error), alternatives []Alternative) func(Profile) ([]Alternative, error) {

	f := func(p Profile) ([]Alternative, error) {
		errs := CheckProfileAlternative(p, alternatives)
		var alternats []Alternative
		var alternat Alternative
		if errs != nil {
			return alternats, errs
		}
		alts, errs := swf(p)
		long := len(alts)
		for i := 0; i < long; i++ {

			comptabilite := MaxCount(alts)

			if len(comptabilite) > 1 {
				alternat, errs = tb(comptabilite)
				alternats = append(alternats, alternat)
				delete(alts, alternat)
			} else {
				alternats = append(alternats, comptabilite[0])
				delete(alts, comptabilite[0])
			}

		}

		return alternats, errs
	}
	return f
}

func SCFFactory(scf func(p Profile) ([]Alternative, error), tb func([]Alternative) (Alternative, error)) func(Profile) (Alternative, error) {

	f := func(p Profile) (alt Alternative, err error) {
		err = CheckProfileAlternative(p, AltsGlobal)
		if err != nil {
			return 0, err
		}
		alts, err := scf(p)
		if len(alts) > 1 {
			alt, err = tb(alts)
		} else {
			alt = alts[0]
		}
		return alt, err

	}

	return f
}
