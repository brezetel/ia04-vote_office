package comsoc

func BordaSWF(p Profile) (count Count, err error) {
	count = make(Count)
	for _, i := range p[0] {
		count[i] = 0
	}
	nbAlternatives := len(p[0])
	for _, v := range p {
		for i2, v2 := range v {
			count[v2] += nbAlternatives - i2 - 1
		}
	}

	return

}
func BordaSCF(p Profile) (bestAlts []Alternative, err error) {
	err = CheckProfileAlternative(p, AltsGlobal)
	if err != nil {
		return
	}

	bordaSwf, _ := BordaSWF(p)
	bestAlts = MaxCount(bordaSwf)
	return

}
