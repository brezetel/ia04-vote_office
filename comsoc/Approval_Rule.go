package comsoc

func ApprovalSWF(p Profile, thresholds []int) (count Count, err error) {

	count = make(Count)
	for _, j := range p[0] {
		count[j] = 0
	}

	for i, v := range p {
		seuil := thresholds[i]
		for x, y := range v {
			if x < seuil {
				count[y] += 1
			}
		}
	}

	return count, err
}
func ApprovalSCF(p Profile, thresholds []int) (bestAlts []Alternative, err error) {
	err = CheckProfileAlternative(p, AltsGlobal)
	if err != nil {
		return
	}

	approval, _ := ApprovalSWF(p, thresholds)
	bestAlts = MaxCount(approval)
	return
}
