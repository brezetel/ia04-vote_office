package comsoc

func CondorcetWinner(p Profile) (bestAlts []Alternative, err error) {

	if err != nil {
		return
	}
	listeAlternatives := p[0]
	for _, uneAlternative := range listeAlternatives {
		gagnant := true

		for _, uneAutreAlternative := range listeAlternatives {
			winner := 0
			loser := 0
			if uneAlternative != uneAutreAlternative {
				for _, v := range p {
					if isPref(uneAlternative, uneAutreAlternative, v) {
						winner += 1
					} else {
						loser += 1
					}
				}
			}
			if winner <= loser && uneAlternative != uneAutreAlternative {
				gagnant = false
			}
		}
		if gagnant {
			bestAlts = append(bestAlts, uneAlternative)
			return
		}
	}

	return
}
