package comsoc

import (
	"errors"
	"fmt"
)

type Alternative int
type Profile [][]Alternative
type Count map[Alternative]int

var AltsGlobal []Alternative

// méthode pour faire un petit test
func Write() {
	fmt.Print("hey")
}

// renvoie l'indice ou se trouve alt dans prefs
func rank(alt Alternative, prefs []Alternative) int {
	for i, v := range prefs {
		if v == alt {
			return i
		}
	}
	return 0
}

// renvoie vrai ssi alt1 est préférée à alt2
func isPref(alt1, alt2 Alternative, prefs []Alternative) bool {
	for _, v := range prefs {
		if v == alt1 {
			return true
		} else if v == alt2 {
			return false
		}
	}
	return false
}

// renvoie les meilleures alternatives pour un décomtpe donné
func MaxCount(count Count) (bestAlts []Alternative) {
	var max int = 0

	for _, value := range count {
		if value > max {
			max = value
		}
	}
	for key, value := range count {
		if value == max {
			bestAlts = append(bestAlts, key)
		}
	}
	return
}
func MinCount(count Count) (bestAlts []Alternative) {
	var min int = 1000000000000

	for _, value := range count {
		if value < min {
			min = value
		}
	}
	for key, value := range count {
		if value == min {
			bestAlts = append(bestAlts, key)
		}
	}
	return
}

// vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative n'apparaît qu'une seule fois par préférences
// func checkProfile(prefs Profile) error {
// 	long := len(prefs[0])
// 	for _, v1 := range prefs {
// 		if long != len(v1) {
// 			return errors.New("ordre non complet")
// 		}
// 		for i2, v2 := range v1 {
// 			for i3, v3 := range v1 {
// 				if v3 == v2 && i3 != i2 {
// 					return errors.New("deux fois la meme preference")
// 				}
// 			}
// 		}
// 	}
// 	return nil
// }

// vérifie les préférences d'un agent, par ex. qu'ils sont tous complets et que chaque alternative n'apparaît qu'une seule fois
func CheckProfile(prefs []Alternative, alts []Alternative) error {
	long := len(prefs)
	if long < len(alts) {
		return errors.New("ordre non complet")
	} else if long > len(alts) {
		return errors.New("il y a trop d'alternatives")
	}
	for _, v := range alts {

		compt := 0
		for _, v3 := range prefs {

			if v == v3 {
				compt++
			}
		}
		if compt != 1 {
			return errors.New("une des alternative n'apparait pas exactement une fois")
		}

	}
	return nil

}

// vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative de alts apparaît exactement une fois par préférences
func CheckProfileAlternative(prefs Profile, alts []Alternative) error {
	for _, v := range prefs {
		err := CheckProfile(v, alts)
		if err != nil {
			return err
		}
	}
	return nil
}
