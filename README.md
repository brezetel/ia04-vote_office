# TP_IA04
## BUREAU DE VOTE_BREZET_RUDENKO

# Utilisation du Bureau de Vote
## Utilisation de l'extension RESTED  
Récupérer le projet (en faisant attention d'avoir un go mod dans le répertoire courant) et installer l'excécutable launch_serveur
```bash
 set GOPRIVATE=gitlab.utc.fr
 go mod init dossier_test
 go get gitlab.utc.fr/brezetel/ia04-vote_office
 go install gitlab.utc.fr/brezetel/ia04-vote_office/cmd/launch_serveur
```
Lancer l'exécutable depuis %USERPROFILE%\go\bin sur Windows ou $HOME/go/bin sur Linux et MacOS
```bash
 launch_serveur.exe
```
Le serveur est démarré et écoute sur http://localhost:8080. 
Aller sur l'extension RESTED et envoyer des requêtes sur /new_ballot, /vote, /result et /count_ballot (voir exemple ci-dessous avec la création d'un nouveau Ballot)

![rest](./pictures/Rested.png)

## Utilisation du Menu dédié
Installer l'exécutable launch_menu depuis le répertoire contenant un go mod et le lancer depuis %USERPROFILE%\go\bin
```bash
 go install gitlab.utc.fr/brezetel/ia04-vote_office/cmd/launch_menu
 launch_menu.exe
```
Un Menu s'ouvre dans le terminal de commandes.  

![Menu](./pictures/Menu.png)  

Il faut commencer par démarrer le serveur. Puis, vous pouvez créer un nouveau bureau de vote en suivant les étapes indiquées. Vous pouvez ensuite créer et faire voter un agent en indiquant ses préférences. Enfin, vous pourrez demander les résultats d'un des bureaux de vote.


# Particularités
* Les méthodes suivantes ont été implémentées : Condorcet, Borda, Copeland, STV, Approbation, Majorité simple
* Les options étant utilisées que pour l'approbation , il n'y a pas d'erreur si aucune option n'est rentrée lors du vote d'un agent pour une autre méthode de vote.
* Si il y a un vainqueur, le gagnant est affiché avec l'ordre du résulat du vote
* Nous affichons 0 et une liste vide, signifiant qu'il n'y a pas de vainqueur dans les cas suivants : 
    + Si il n'existe pas de Gagnant de Condorcet
    + Si la deadline est passée et qu'il n'y a pas eu de vote
* Lors de la création du bureau de vote, l'id du bureau est renvoyé
* La fonction SWFFactory est utilisée pour toutes les méthodes sauf pour Condorcet, Approbation et STV : 
    + Condorcet : le tiebreak n'est pas utilisé et nous devons uniquement renvoyer le gagnant
    + Approbation : les alternatives renvoyées sont celles qui sont approuvées, dont le nombre est défini par l'option
    + STV : il faut effectuer les transferts des votes correctement et inclure un tie break dans la fonction STV
