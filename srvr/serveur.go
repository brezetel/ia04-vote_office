package srvr

import (
	"errors"
	"strconv"
	"strings"

	"gitlab.utc.fr/brezetel/ia04-vote_office/comsoc"

	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"sync"
	"time"
)

type RestServerAgent struct {
	sync.Mutex
	id        string
	addr      string
	nbBallots int
	ballots   map[string]*BallotAgent
}

type BallotAgent struct {
	sync.Mutex
	ID       string
	Rule     string
	Deadline time.Time
	VoterIDs map[string]bool
	Options  []int
	Alts     []comsoc.Alternative
	TieBreak []int
	Profile  comsoc.Profile
}

func NewRestServerAgent(addr string) *RestServerAgent {
	return &RestServerAgent{
		id:      addr,
		addr:    addr,
		ballots: make(map[string]*BallotAgent),
	}
}

// Test de la méthode
func (rsa *RestServerAgent) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusMethodNotAllowed)
		// fmt.Fprintf(w, "method %q not allowed", r.Method)
		return false
	}
	return true
}

func (rsa *RestServerAgent) decodeRequestBallot(r *http.Request) (req comsoc.RequestNewBallot, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req) // met dans req la réponse du reader de la requete et si erreur enregistre l'erreur
	return
}
func (rsa *RestServerAgent) decodeRequestVote(r *http.Request) (req comsoc.RequestVote, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req) // met dans req la réponse du reader de la requete et si erreur enregistre l'erreur
	return
}

func (rsa *RestServerAgent) decodeRequestResult(r *http.Request) (req comsoc.RequestResult, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req) // met dans req la réponse du reader de la requete et si erreur enregistre l'erreur
	return
}

// fonction qui permet de verifier une requette de création d'un bureau de vote
func validateNewBallotRequest(req *comsoc.RequestNewBallot) error {
	req.Rule = strings.ToLower(strings.TrimSpace(req.Rule))
	if req.Rule != "majority" && req.Rule != "borda" && req.Rule != "approval" && req.Rule != "stv" && req.Rule != "copeland" && req.Rule != "condorcet" {
		return errors.New("not implemented")
	}

	deadline, err := time.Parse(time.RFC3339, strings.TrimSpace(req.Deadline))
	if err != nil {
		return errors.New("le format de la deadline n'est pas pris en charge")
	}
	if deadline.Before(time.Now().In(time.FixedZone("UTC+1", 60*60))) {
		return errors.New("la deadline ne doit pas être passé ")
	}

	if len(req.VoterIDs) == 0 {
		return errors.New("aucun votant n'est spécifié")
	}

	if req.Alts < 2 {
		return errors.New("nous avons besoin de au moins deux altérnatives")
	}

	return nil
}

// fonction qui permet de créer un nouveau bureau de vote

func (rsa *RestServerAgent) createNewBallot(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	if !rsa.checkMethod("POST", w, r) {
		return
	}

	req, err := rsa.decodeRequestBallot(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest) // met l'erreur dans le retour
		return
	}

	err2 := validateNewBallotRequest(&req)

	if err2 != nil {
		if err2.Error() == "not implemented" {
			w.WriteHeader(http.StatusNotImplemented)
		} else {
			w.WriteHeader(http.StatusBadRequest)
		}
		w.Write([]byte(err2.Error()))
		return

	}
	deadLine, _ := time.Parse(time.RFC3339, strings.TrimSpace(req.Deadline))

	id := "scrutin" + strconv.Itoa(rsa.nbBallots)
	var votersMap map[string]bool = make(map[string]bool)
	var optionsMap []int
	for _, voters := range req.VoterIDs {
		votersMap[voters] = false
	}
	if len(votersMap) < len(req.VoterIDs) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Agents avec le meme ID"))
		return
	}
	var alternatives []comsoc.Alternative
	var tieBreakTest []comsoc.Alternative

	for i := 1; i <= req.Alts; i++ {
		alternatives = append(alternatives, comsoc.Alternative(i))
		tieBreakTest = append(tieBreakTest, comsoc.Alternative(req.TieBreak[i-1]))
	}
	err3 := comsoc.CheckProfile(tieBreakTest, alternatives)
	if err3 != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err3.Error()))
		return
	}
	ballot := &BallotAgent{
		ID:       id,
		Rule:     req.Rule,
		Deadline: deadLine,
		VoterIDs: votersMap,
		Alts:     alternatives,
		TieBreak: req.TieBreak,
		Options:  optionsMap,
	}

	rsa.ballots[ballot.ID] = ballot
	rsa.nbBallots++
	resp := comsoc.NewBallotResponse{

		BallotId: id,
	}

	w.WriteHeader(http.StatusCreated)
	jsonResp, _ := json.Marshal(resp)
	w.Write(jsonResp)
}

func determineWinner(ballot *BallotAgent) (resp comsoc.ResponseWinner, err error) {

	if len(ballot.Profile) == 0 {
		resultRanking := make([]int, 0)

		resp = comsoc.ResponseWinner{
			Winner:  0,
			Ranking: resultRanking,
		}
		return resp, nil
	}

	var tieBreak []comsoc.Alternative
	var resultRanking []int
	var rule func(comsoc.Profile) ([]comsoc.Alternative, error)
	for _, i := range ballot.TieBreak {
		tieBreak = append(tieBreak, comsoc.Alternative(i))
	}
	if ballot.Rule == "borda" {
		rule = comsoc.SWFFactory(comsoc.BordaSWF, comsoc.TieBreakFactory(tieBreak), ballot.Alts)
	}
	if ballot.Rule == "majority" {
		rule = comsoc.SWFFactory(comsoc.MajoritySWF, comsoc.TieBreakFactory(tieBreak), ballot.Alts)
	}

	if ballot.Rule == "copeland" {
		rule = comsoc.SWFFactory(comsoc.CopelandSWF, comsoc.TieBreakFactory(tieBreak), ballot.Alts)
	}
	if ballot.Rule == "stv" {

		rule = func(p comsoc.Profile) ([]comsoc.Alternative, error) {
			errs := comsoc.CheckProfileAlternative(p, ballot.Alts)
			var alternats []comsoc.Alternative
			if errs != nil {
				return alternats, errs
			}
			alts, err := comsoc.STV_SWF(p, comsoc.TieBreakSTVFactory(tieBreak))
			if err != nil {
				return alternats, errs
			}
			long := len(alts)
			for i := 0; i < long; i++ {
				comptabilite := comsoc.MaxCount(alts)
				alternats = append(alternats, comptabilite[0])
				delete(alts, comptabilite[0])
			}
			return alternats, err
		}

	}
	if ballot.Rule == "condorcet" {
		rule = func(p comsoc.Profile) ([]comsoc.Alternative, error) {
			errs := comsoc.CheckProfileAlternative(p, ballot.Alts)
			var alternats []comsoc.Alternative
			if errs != nil {
				return alternats, errs
			}
			return comsoc.CondorcetWinner(p)
		}

	}
	if ballot.Rule == "approval" {
		tb := comsoc.TieBreakFactory(tieBreak)
		rule = func(p comsoc.Profile) ([]comsoc.Alternative, error) {
			errs := comsoc.CheckProfileAlternative(p, ballot.Alts)
			var alternats []comsoc.Alternative
			var alternat comsoc.Alternative
			if errs != nil {
				return alternats, errs
			}
			alts, errs := comsoc.ApprovalSWF(p, ballot.Options)

			long := len(alts)
			for i := 0; i < long; i++ {

				comptabilite := comsoc.MaxCount(alts)

				if len(comptabilite) > 1 {
					alternat, errs = tb(comptabilite)
					alternats = append(alternats, alternat)
					delete(alts, alternat)
				} else {
					alternats = append(alternats, comptabilite[0])
					delete(alts, comptabilite[0])
				}

			}

			return alternats, errs
		}
	}

	result, err := rule(ballot.Profile)

	if err != nil {
		resp := comsoc.ResponseWinner{
			Winner:  0,
			Ranking: nil,
		}
		return resp, err
	}
	if len(result) == 0 {
		resp = comsoc.ResponseWinner{
			Winner:  0,
			Ranking: nil,
		}
	} else {

		for _, i := range result {
			resultRanking = append(resultRanking, int(i))
		}
		resp = comsoc.ResponseWinner{
			Winner:  int(result[0]),
			Ranking: resultRanking,
		}
	}

	return resp, nil
}

// fonction qui récupere l'id du bureaux de vote et détermine le gagnant a partir du choix des regles
func (rsa *RestServerAgent) getWinner(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	if !rsa.checkMethod("POST", w, r) {
		return
	}

	req, err := rsa.decodeRequestResult(r)
	if err != nil {
		// fmt.Println(" On a décodé la requette et l'id demandé est ", req.Ballotid)
		w.WriteHeader(http.StatusBadRequest) // met l'erreur dans le retour
		//fmt.Fprint(w, err.Error())
		return
	}

	ballotID := req.Ballotid
	if ballotID == "" {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("l'id du ballot est nécessaire"))
		return
	}

	ballot, exists := rsa.ballots[ballotID]
	if !exists {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("l'id du balot n'a pas été trouvé"))
		return
	}
	if !time.Now().After(ballot.Deadline) {
		w.WriteHeader(http.StatusTooEarly)
		w.Write([]byte("Erreur 425: Too Early"))
		return
	}

	resp, err := determineWinner(ballot)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusAccepted)
	data, _ := json.Marshal(resp)
	w.Write(data)

}

// fonction qui enregistre un vote sur un des bureaux de vote
func (rsa *RestServerAgent) enregistrerVoteBallot(w http.ResponseWriter, r *http.Request) {

	rsa.Lock()
	defer rsa.Unlock()

	if !rsa.checkMethod("POST", w, r) {
		return
	}

	req, err := rsa.decodeRequestVote(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		// fmt.Fprint(w, err.Error())
		return
	}

	ballotID := req.Vote_id

	if ballotID == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("ballot_id required"))
		return
	}

	ballot, exists := rsa.ballots[ballotID]
	if !exists {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid ballot_id"))
		return
	}

	ballot.Lock()
	defer ballot.Unlock()

	if time.Now().After(ballot.Deadline) {
		w.WriteHeader(http.StatusServiceUnavailable)
		w.Write([]byte("la deadline est dépassée"))
		return
	}

	_, exist := ballot.VoterIDs[req.Agent_id]
	if !exist {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Agent non autorisé"))
		return
	}

	hasVoted := ballot.VoterIDs[req.Agent_id]
	if hasVoted {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte("vote déjà effectué"))
		return
	}

	var alternativeSlice []comsoc.Alternative
	for _, val := range req.Prefs {
		alternativeSlice = append(alternativeSlice, comsoc.Alternative(val))
	}

	err2 := comsoc.CheckProfile(alternativeSlice, ballot.Alts)
	if err2 != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Les préferences du votes ne sont pas valides"))
		return
	}

	ballot.Profile = append(ballot.Profile, alternativeSlice)

	if len(req.Option) > 0 {
		ballot.Options = append(ballot.Options, req.Option[0])
	} else {
		if ballot.Rule == "approval" {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Nous avons besoin d'information supplémentaire pour le bureau de vote avec une règle approval"))
			return
		}
	}
	ballot.VoterIDs[req.Agent_id] = true

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("vote pris en compte"))
}

// fonctions qui affiche simplement le nombre de bureaux de votes
func (rsa *RestServerAgent) getBallotCount(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	if !rsa.checkMethod("GET", w, r) {
		return
	}

	count := len(rsa.ballots) // Assume `ballots` is a map or slice. Adapt as needed.

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(count)
	w.Write(serial)
}

// Fonction qui permet de lancer le serveur

func (rsa *RestServerAgent) Start() (err error) {
	// création du multiplexer
	mux := http.NewServeMux()
	mux.HandleFunc("/vote", rsa.enregistrerVoteBallot)
	mux.HandleFunc("/result", rsa.getWinner)
	mux.HandleFunc("/new_ballot", rsa.createNewBallot)
	mux.HandleFunc("/count_ballot", rsa.getBallotCount)
	// création du serveur http
	s := &http.Server{
		Addr:           rsa.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	// lancement du serveur
	log.Println("Le serveur avec l'id", rsa.id, " a démarré.")
	log.Println("Il est en écoute sur : ", rsa.addr)
	go log.Fatal(s.ListenAndServe())

	return
}
